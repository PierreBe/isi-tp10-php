<?php

include_once 'sql.php';

include_once 'checkSession.php';

$dateCreation = '';
$dateModification = date('Y-m-d h:i:s');

$userData = ['firstName' => '', 'lastName' => '', 'email' => '', 'userName' => ''];

if (isset($_GET['id'])) {
    $userData = getUser($_GET['id']);
} else {
    if (isset($_POST['prenom']) && isset($_POST['nom']) && isset($_POST['courriel']) && isset($_POST['userName'])) {
        if (isset($_POST['id'])) {
            if (isset($_POST['password'])) {
                updateUser($_POST['id'], $_POST['prenom'], $_POST['nom'], $_POST['courriel'], $_POST['userName'], $dateModification, $_POST['password']);
            } else {
                updateUser($_POST['id'], $_POST['prenom'], $_POST['nom'], $_POST['courriel'], $_POST['userName'], $dateModification);
            }
        } else {
            insertUser($_POST['prenom'], $_POST['nom'], $_POST['courriel'], $_POST['userName'], $_POST['password'], date('Y-m-d h:i:s'), $dateModification);
        }
    }
}

?>

<!DOCTYPE html>
<html>

<head>
    <?php include_once 'head.php'; ?>
</head>

<body>
    <div class="container">
        <?php include_once 'nav.php'; ?>
        <form action="form.php" method="post">
            <div class="form-group row">
                <label for="prenom" class="col-sm-2 col-form-label">Prénom</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="prenom" name="prenom" value='<?=$userData['firstName']; ?>'>
                </div>
            </div>
            <div class="form-group row">
                <label for="nom" class="col-sm-2 col-form-label">Nom</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="nom" name="nom" value='<?=$userData['lastName']; ?>'>
                </div>
            </div>
            <div class="form-group row">
                <label for="courriel" class="col-sm-2 col-form-label">Courriel</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" id="courriel" name="courriel" value='<?=$userData['email']; ?>'>
                </div>
            </div>
            <div class="form-group row">
                <label for="userName" class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="userName" name="userName" value='<?=$userData['userName']; ?>'>
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    <?php if (isset($_GET['id'])):  ?>

                    <input type="hidden" name='id' value='<?= $_GET['id']; ?>'>
                    <?php endif; ?>
                    <input type="password" class="form-control" id="password" name="password">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="submit" class="btn btn-primary">Sauvegarder</button>
                </div>
            </div>
        </form>
    </div>
</body>

</html>
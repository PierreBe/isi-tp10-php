<?php

// infos db :
define('DB_HOST', 'programmation-web-3.org');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_NAME', 'ProgrammationWeb3');

function getCollectionFromSQLDB($sqlQuery)
{
    $mysqli = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

    if (mysqli_connect_errno()) {
        echo 'Erreur de connection au serveur MySQL: ('.$mysqli->connect_errno.') '.$mysqli->connect_error;
        exit;
    }

    mysqli_set_charset($mysqli, 'utf8');

    $result = mysqli_query($mysqli, $sqlQuery);

    if (!$result) {
        $error = mysqli_error($mysqli);
    }

    $collection = [];

    while ($associativeArray = mysqli_fetch_assoc($result)) {
        array_push($collection, $associativeArray);
    }

    mysqli_free_result($result);

    mysqli_close($mysqli);

    return $collection;
}

function fillTable()
{
    $ans = '';

    $collection = getCollectionFromSQLDB('select firstName,lastName,email,creationDate,modificationDate,id from tp_user');

    foreach ($collection as $row) {
        $ans .= '<tr>';
        $rows = array_keys($row);
        $lastIndex = count($row) - 1;
        for ($i = 0; $i < $lastIndex; ++$i) {
            $ans .= '<td>'.$row[$rows[$i]].'</td>';
        }
        $ans .= '<td><a href=\'form.php?id='.$row[$rows[$lastIndex]].'\'><i class="fas fa-pen-square"></i></a><a href=\'index.php?id='.$row[$rows[$lastIndex]].'\'><i class="fas fa-window-close"></i></a></td>';    // <i class="fas fa-pencil-alt"></i><i class="fas fa-times"></i>
        $ans .= '</tr>';
    }

    return $ans;
}

function checkUser($userName, $password)
{
    $ans = false;
    $collection = getCollectionFromSQLDB('select userPassword from tp_user where userName='.'\''.$userName.'\'');
    $i = -1;
    while (!$ans && ++$i < count($collection)) {
        if (password_verify($password, $collection[$i]['userPassword'])) {
            $ans = true;
        }
    }

    return $ans;
}

function getUser($id)
{
    return getCollectionFromSQLDB('select firstName,lastName,email,userName from tp_user where id=\''.$id.'\'')[0];
}

function deleteUser($id)
{
    $mysqli = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

    if (mysqli_connect_errno()) {
        echo 'Erreur de connection au serveur MySQL: ('.$mysqli->connect_errno.') '.$mysqli->connect_error;
        exit;
    }

    mysqli_set_charset($mysqli, 'utf8');

    $result = mysqli_query($mysqli, 'delete from tp_user where id=\''.$id.'\'');

    if (!$result) {
        $error = mysqli_error($mysqli);
    }

    mysqli_close($mysqli);
}

function insertUser($prenom, $nom, $courriel, $userName, $password, $dateCreation, $dateModification)
{
    $password = password_hash($password, PASSWORD_DEFAULT);

    $sql = "insert INTO tp_user (firstName, lastName,email,userName,userPassword,creationDate,modificationDate) VALUES ('".$prenom."', '".$nom."','".$courriel."','".$userName."','".$password."','".$dateCreation."','".$dateModification."');";

    $status = '';

    $mysqli = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);

    mysqli_set_charset($mysqli, 'utf8');

    if (mysqli_connect_errno()) {
        echo 'Erreur de connection au serveur MySQL: ('.$mysqli->connect_errno.') '.$mysqli->connect_error;
        exit;
    }

    if (mysqli_query($mysqli, $sql)) {
        $status = 'reussi!';
    } else {
        $status = 'Erreur! '.mysqli_error($$mysqli);
    }

    mysqli_close($mysqli);

    return $status;
}

function updateUser($id, $prenom, $nom, $courriel, $userName, $dateModification, $password = null)
{
    $updateRequete = "update tp_user set firstName = '$prenom' ,lastName = '$nom',email='$courriel',userName='$userName'".($password ? ",    userPassword ='".password_hash($password, PASSWORD_DEFAULT)."'" : '').",modificationDate = '$dateModification' where id= '$id';";

    $status = '';

    $mysqli = mysqli_connect(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_NAME);
    mysqli_set_charset($mysqli, 'utf8');

    if (mysqli_connect_errno()) {
        echo 'Erreur de connection au serveur MySQL: ('.$mysqli->connect_errno.') '.$mysqli->connect_error;
        exit;
    }

    if (mysqli_query($mysqli, $updateRequete)) {
        $status = 'reussi!';
    } else {
        $status = 'Error!   '.mysqli_error($$mysqli);
    }

    mysqli_close($mysqli);

    return $status;
}

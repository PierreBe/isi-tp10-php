<?php

include_once 'sql.php';

include_once 'checkSession.php';

if (isset($_GET['logout'])) {
    session_destroy();
    header('Location: login.php');
} elseif (isset($_GET['id'])) {
    deleteUser($_GET['id']);
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include_once 'head.php'; ?>
    <script src="https://kit.fontawesome.com/7addcbd6e4.js"></script>
</head>

<body>
    <div class="container">
        <?php include_once 'nav.php'; ?>
        <table class="table table-bordered table-hover !table-sm">
            <thead>
                <tr>
                    <th>Prénom</th>
                    <th>Nom</th>
                    <th>Courriel</th>
                    <th>Date de création</th>
                    <th>Date de modification</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?=fillTable(); ?>
            </tbody>
        </table>
    </div>
</body>

</html>
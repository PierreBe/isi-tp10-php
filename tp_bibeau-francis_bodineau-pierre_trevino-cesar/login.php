<?php

include_once 'sql.php';

if (isset($_POST['userName']) && isset($_POST['password']) && checkUser($_POST['userName'], $_POST['password'])) {
    session_start();
    $_SESSION['userName'] = $_POST['userName'];
    header('Location: index.php');
}
?>

<!DOCTYPE html>
<html>

<head>
    <?php include_once 'head.php'; ?>
</head>

<body>
    <div class="container">
        <div class="jumbotron">
            <h1>Login</h1>
            <p>L'usager par défaut est <b>userName : admin</b> et <b>password : admin</b></p>
            <hr>
            <form action='' method='post'>
                <div class="form-group row">
                    <label for="userName" class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="userName" name="userName">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>

</html>